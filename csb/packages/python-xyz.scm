(define-module (csb packages python-xyz)
  #:use-module (guix i18n)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages machine-learning)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages base)
  #:use-module (gnu packages astronomy)
  #:use-module (gnu packages check))

(define-public python-pykg-config
  (package
    (name "python-pykg-config")
    (version "1.3.0")
    (source (origin
	      (method url-fetch)
	      (uri (pypi-uri "pykg-config" version))
	      (sha256
	       (base32
		"091nvkixi3k2ymfj9p2lmpfk60wpf6i7aw5bf18bq2rrqh1n2r4w"))))
    (build-system python-build-system)
    (home-page "http://github.com/gbiggs/pykg-config")
    (synopsis "pkg-config replacement.")
    (description "pkg-config replacement.")
    (license #f)))

(define-public python-healpy
  (package
    (name "python-healpy")
    (version "1.16.1")
    (source (origin
	      (method url-fetch)
	      (uri (pypi-uri "healpy" version))
	      (sha256
	       (base32
		"0bwxlvfnw7mp5w15bbh3rb4gq3345bc3k7g05mkrkxpxfw51nsbd"))))
    (build-system python-build-system)
    (propagated-inputs (list python-astropy
			     python-matplotlib
			     python-numpy
			     python-cython
			     pykg-config
			     python-scipy))
    (home-page "http://github.com/healpy")
    (synopsis "Healpix tools package for Python")
    (description "Healpix tools package for Python")
    (license #f)))

(define-public python-python-lisagwresponse
  (package
   (name "python-lisagwresponse")
   (version "2.1.2")
   (source (origin
	    (method url-fetch)
	    (uri (pypi-uri "lisagwresponse" version))
	    (sha256
	     (base32
	      "0pfaavhm95zg2xfg9rp58v5j54myy2zxzx2c875c0w0f88hfhh60"))))
   (build-system python-build-system)
   (propagated-inputs (list python-h5py
			    python-healpy
			    python-importlib-metadata
			    python-lisaconstants
			    python-matplotlib
			    python-numpy
			    python-packaging
			    python-scipy))
   (home-page "https://gitlab.in2p3.fr/lisa-simulation/gw-response")
   (synopsis
    "LISA GW Response generates the instrumental response to gravitational-waves, and produces a gravitational-wave file compatible with LISANode.")
   (description
    "LISA GW Response generates the instrumental response to gravitational-waves, and
produces a gravitational-wave file compatible with LISANode.")
   (license #f)))

(define-public python-pytdi
  (package
   (name "python-pytdi")
   (version "1.2.1")
   (source (origin
	    (method url-fetch)
	    (uri (pypi-uri "pytdi" version))
	    (sha256
	     (base32
	      "0mrchxys8bhz5l38dbyymswz4aghjcp7gakr5ihi5hgvs7vxszd6"))))
   (build-system python-build-system)
   (propagated-inputs (list python-numpy python-packaging python-pytest python-h5py python-scipy))
   (home-page "https://gitlab.in2p3.fr/LISA/LDPG/wg6_inrep/pytdi")
   (synopsis "Python implementation of time-delay interferometry algorithms.")
   (description
    "Python implementation of time-delay interferometry algorithms.")
   (license #f)))

(define-public python-glasflow
  (package
   (name "python-glasflow")
   (version "0.1.2")
   (source (origin
	    (method url-fetch)
	    (uri (pypi-uri "glasflow" version))
	    (sha256
	     (base32
	      "0nyc8742i1a29hrgw9hjkyis002ymc41f57hg6sa6qqcq8h89zz1"))))
   (build-system python-build-system)
   (propagated-inputs (list python-numpy python-pytorch))
   (native-inputs (list python-black
			python-pre-commit
			python-pytest
			python-pytest-cov
			;; python-pytest-integration
			;; python-pytest-rerunfailures
			;; python-torchtestcase
			;; python-umnn
			))
   (home-page "https://github.com/igr-ml/glasflow")
   (synopsis "Normalising flows using nflows")
   (description "Normalising flows using nflows")
   (license #f)))

(define-public python-nessai
  (package
   (name "python-nessai")
   (version "0.7.0")
   (source (origin
	    (method url-fetch)
	    (uri (pypi-uri "nessai" version))
	    (sha256
	     (base32
	      "0bpcv131mmazm00vh5h15brln5y1sm8r9pbq8vfps7n7xdz5blah"))))
   (build-system python-build-system)
   (propagated-inputs (list python-glasflow
			    python-matplotlib
			    python-numpy
			    python-pandas
			    python-scipy
			    python-seaborn
			    python-pytorch
			    python-tqdm))
   (native-inputs (list python-corner
			python-pre-commit
			python-pytest
			python-pytest-cov
			;; python-pytest-integration
			;; python-pytest-rerunfailures
			;; python-pytest-timeout
			;; python-ray
			))
   (home-page "https://github.com/mj-will/nessai")
   (synopsis "Nessai: Nested Sampling with Aritificial Intelligence")
   (description "Nessai: Nested Sampling with Aritificial Intelligence")
   (license #f)))

(define-public python-kombine
  (package
   (name "python-kombine")
   (version "0.8.4")
   (source (origin
	    (method url-fetch)
	    (uri (pypi-uri "kombine" version))
	    (sha256
	     (base32
	      "10nkmy69d8dhwh0pmpvwga8gvd9x9wjsw6i4nxc31nbf21ggbhag"))))
   (build-system python-build-system)
   (propagated-inputs (list python-numpy python-scipy))
   (home-page "https://github.com/bfarr/kombine")
   (synopsis
    "An embarrassingly parallel, kernel-density-based                 ensemble sampler")
   (description
    "An embarrassingly parallel, kernel-density-based ensemble sampler")
   (license #f)))

(define-public python-dnest4
  (package
   (name "python-dnest4")
   (version "0.2.4")
   (source (origin
	    (method url-fetch)
	    (uri (pypi-uri "dnest4" version))
	    (sha256
	     (base32
	      "11sk2dkmpywvgrnc6fwlrahh959c7sl2135dd5cw969m84r4a5pc"))))
   (build-system python-build-system)
   (propagated-inputs (list python-numpy
			    python-cython))
   (home-page "https://github.com/eggplantbren/DNest4")
   (synopsis "Diffusive nested sampling in Python")
   (description "Diffusive nested sampling in Python")
   (license #f)))

(define-public python-cpnest
  (package
   (name "python-cpnest")
   (version "0.11.4")
   (source (origin
	    (method url-fetch)
	    (uri (pypi-uri "cpnest" version))
	    (sha256
	     (base32
	      "0s5n7n3q00215zry7mx2jpv257y6w4ncqa49xb5r55g3125zikxf"))))
   (build-system python-build-system)
   ;; TODO: trop lent, réactiver
   (arguments
    `(#:phases
      (modify-phases %standard-phases
		     (delete 'check))))
   (propagated-inputs (list python-corner
			    python-cython
			    python-matplotlib
			    python-numpy
			    python-scipy
			    python-setuptools-scm
			    python-tqdm))
   (home-page "https://github.com/johnveitch/cpnest")
   (synopsis "CPNest: Parallel nested sampling")
   (description "CPNest: Parallel nested sampling")
   (license #f)))

(define-public python-bilby.cython
  (package
   (name "python-bilby.cython")
   (version "0.4.0")
   (source (origin
	    (method url-fetch)
	    (uri (pypi-uri "bilby.cython" version))
	    (sha256
	     (base32
	      "1n7plvd370l0s2a7asihi3xm2al6srgkhz0ajscpb23wwk4nypvn"))))
   (build-system python-build-system)
   ;; WARNING: TESTS FAILS !!
   (arguments
    `(#:phases
      (modify-phases %standard-phases
		     (delete 'check)
		     (delete 'sanity-check))))
   (propagated-inputs (list python-numpy
			    python-cython
			    python-setuptools-scm))
   (home-page "https://git.ligo.org/colm.talbot/bilby-cython")
   (synopsis "Optimized functionality for Bilby")
   (description "Optimized functionality for Bilby")
   (license #f)))



(define-public python-corner
  (package
   (name "python-corner")
   (version "2.2.1")
   (source (origin
	    (method url-fetch)
	    (uri (pypi-uri "corner" version))
	    (sha256
	     (base32
	      "1rai8yk4b9zw644szi7l189xf6p5fcsv503dl0mbp4qhsfd1v7yk"))))
   (build-system python-build-system)
   (propagated-inputs (list python-matplotlib))
   (native-inputs (list
		   ;; python-arviz
		   python-black
		   python-flake8
		   python-isort
		   ;; python-myst-nb
		   python-pypandoc
		   python-pep517
		   python-pre-commit
		   python-pytest
		   python-pytest-cov
		   python-sphinx
		   ;; python-sphinx-book-theme
		   python-toml
		   python-twine))
   (home-page "https://corner.readthedocs.io")
   (synopsis "Make some beautiful corner plots")
   (description "Make some beautiful corner plots")
   (license #f)))

(define-public python-bilby
  (package
   (name "python-bilby")
   (version "1.3.0")
   (source (origin
	    (method url-fetch)
	    (uri (pypi-uri "bilby" version))
	    (sha256
	     (base32
	      "12z9lacnmndrrgmjrq7w4m7zfbaibaimr3nkfb3alsiipvh9nqvf"))))
   (build-system python-build-system)
   (propagated-inputs (list
		       python-attrs
		       python-bilby.cython
		       python-corner
		       python-dill
		       python-dynesty
		       python-cpnest
		       python-dnest4
		       python-kombine
		       python-setuptools-scm
		       python-emcee
		       python-h5py
		       python-matplotlib
		       python-numpy
		       python-pandas
		       python-scipy
		       python-tqdm))
   (home-page "https://git.ligo.org/lscsoft/bilby")
   (synopsis "A user-friendly Bayesian inference library")
   (description
    "This package provides a user-friendly Bayesian inference library")
   (license #f)))

(define-public python-cocotb
  (package
   (name "python-cocotb")
   (version "1.6.1")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
	   (url "https://github.com/cocotb/cocotb.git")
	   (commit (string-append "v" version))))
     (file-name (git-file-name name version))
     (sha256
      (base32
       "18hpxrs09pmpzbzvw338aq0r5w0cc22pcc6gjl1ij2bb9rdkhd0b"))))
   (build-system python-build-system)
   (arguments
    `(#:tests? #f			; no test suite
      ))
   (native-inputs (list gnu-make gcc-toolchain))
   (home-page "https://github.com/cocotb/cocotb.git")
   (synopsis
    "Cocotb is a COroutine based COsimulation TestBench environment for verifying VHDL and SystemVerilog RTL using Python.")
   (description
    "With cocotb, VHDL or SystemVerilog are normally only used for the design itself, not the testbench.")
   (license #f)))
